import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Todo {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  title: string;

  @Column({ nullable: false })
  status: boolean;

  // @CreateDateColumn()
  // createdAt?;
  //
  // @UpdateDateColumn()
  // updatedAt?;
  //
  // @DeleteDateColumn()
  // deletedAt?;
}
