import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Todo } from '../entities/todo.entity';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(Todo) private readonly todoRepository: Repository<Todo>,
  ) {}

  getAll() {
    return this.todoRepository.find();
  }

  createTodo(todo): Promise<InsertResult> {
    return this.todoRepository.insert(todo);
  }

  updateTodo(todo, id): Promise<UpdateResult> {
    return this.todoRepository.update(id, todo);
  }

  deleteTodo(id): Promise<DeleteResult> {
    return this.todoRepository.delete(id);
  }
}
