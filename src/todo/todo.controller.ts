import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { TodoService } from './todo.service';
import { Todo } from '../entities/todo.entity';
import { DeleteResult, InsertResult, UpdateResult } from 'typeorm';
import { CreateTodoDto } from '../dto/createTodo.dto';

@Controller('todo')
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Get()
  getAll(): Promise<Todo[]> {
    return this.todoService.getAll();
  }

  @Post()
  createTodo(@Body() todo: CreateTodoDto): Promise<InsertResult> {
    return this.todoService.createTodo(todo);
  }

  @Put(':id')
  updateTodo(@Body() todo, @Param('id') id: number): Promise<UpdateResult> {
    return this.todoService.updateTodo(todo, id);
  }

  @Delete(':id')
  deleteTodo(@Param('id') id: number): Promise<DeleteResult> {
    return this.todoService.deleteTodo(id);
  }
}
