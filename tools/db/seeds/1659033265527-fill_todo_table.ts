import { MigrationInterface, QueryRunner } from 'typeorm';
import { Todo } from '../../../src/entities/todo.entity';

export class fillTodoTable1659033265527 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.save(
      queryRunner.manager.create<Todo>(Todo, [
        {
          id: 1,
          title: 'Todo 1',
          status: false,
        },
        {
          id: 2,
          title: 'Todo 2',
          status: false,
        },
      ]),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.clearTable('todo');
  }
}
