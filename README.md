# TODO APP

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

## DATABASE

```bash
# start db
$ docker-compose -f ./tools/docker/docker-compose.yml up -d
```

### Migrations

Создание чистой миграции для ручного заполнения

```bash
yarn run typeorm migration:create -n <migration-name>
```

Применить миграции

```bash
yarn run typeorm migration:run
```

### Seeds

Инициализация пустого сида

```bash
yarn run seeds migration:create -n <seed-name>
```

Запустить сиды

```bash
yarn run seeds migration:run
```
